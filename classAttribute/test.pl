#!/usr/bin/perl
use warnings;
use strict;
use ClassA;
use ClassB;

my $a = ClassA->new('name' => 'A');
print $a->debug(), "\n";
ClassA->debug(1);
my $o = ClassB->new('init' => 1);
$o->data();
print $a->debug(), "\n";

